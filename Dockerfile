FROM alpine:3.12

RUN apk update && apk upgrade
RUN apk add gcc python3 py3-pip libc-dev g++ python3-dev

# set the working directory in the container
WORKDIR /app

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY src/ .

EXPOSE 1337

# command to run on container start
CMD [ "python3", "-u", "./server.py" ] 
