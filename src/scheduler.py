import random
import requests
from threading import Thread


class Scheduler:
    # List of docker clients
    def __init__(self, ips):
        self.worker_ips = ips
        self.ready_ip = self._get_ip()
        Thread(target=self.ensure_ip).start()

    def register_worker(self, ip):
        if ip not in self.worker_ips:
            self.worker_ips.append(ip)
            print("Added a new worker:", ip)

    def ensure_ip(self):
        while True:
            if self.ready_ip is None and self.worker_ips:
                self.ready_ip = self._get_ip()
                print(f"Set ready_ip to {self.ready_ip}")

    def _get_ip(self):
        while self.worker_ips:
            try:
                worker_ip = random.choice(self.worker_ips)

                res = requests.get(
                    f"http://{worker_ip}:9000/hooks/container", timeout=5
                )
                if res.status_code != 200:
                    # worker is dead
                    self.worker_ips.remove(worker_ip)
                else:
                    # yay, IP!
                    return res.text
            except:
                # worker is dead
                self.worker_ips.remove(worker_ip)

    def schedule_container(self):
        if self.ready_ip is None:
            raise Exception("I'm out of compute man")
        else:
            ip = self.ready_ip
            self.ready_ip = None
            return ip
