import time
from dnslib import *
from dnslib.server import DNSServer
from scheduler import Scheduler

cached_ips = {}


class Resolver:
    scheduler = None

    def __init__(self, scheduler):
        self.scheduler = scheduler

    def resolve(self, request, handler):
        reply = request.reply()

        for question in request.questions:
            domain = extract_domain(question)
            if "cloud.pyjam.as" not in domain:
                continue

            # Handles questions that are pns (pyjamas name service)
            # Example for adding an ipv4
            # `dig -t txt @localhost -p 1337 legacy-42-42-42-15.cloud.pyjam.as`
            # and for ipv6
            # `dig -t txt @localhost -p 1337 0-0-0-0-0-0-0-1.cloud.pyjam.as`
            if question.qtype == 16:  # 16 is a TXT record
                pns_encoded_ip = domain.split(".")[0]
                values = pns_encoded_ip.split("-")
                if values[0] == "legacy":
                    ip = ".".join(values[1:])
                    # TODO: Throw an error if an invalid ip submitted
                    self.scheduler.register_worker(ip)
                else:
                    # TODO: Throw an error if an invalid ip submitted
                    ip = ":".join(values)
                    self.scheduler.register_worker(ip)

            else:  # maybe only handle [1, 28] (A and AAAA records)
                container_ip = cached_ips.get(domain)

                try:
                    if container_ip is None:
                        container_ip = self.scheduler.schedule_container()
                        cached_ips[domain] = container_ip

                    reply.add_answer(RR(domain, rdata=A(container_ip)))
                except Exception as e:
                    reply.add_answer(RR(domain, QTYPE.TXT, rdata=TXT(str(e).encode())))

        return reply


def extract_domain(question):
    return str(question.qname)[:-1]


if __name__ == "__main__":
    scheduler = Scheduler(["0.0.0.0"])

    resolver = Resolver(scheduler)
    server = DNSServer(resolver, port=1337, address="0.0.0.0")

    print("DNS Server ready!")
    server.start_thread()

    while True:
        time.sleep(5)

    server.stop()
